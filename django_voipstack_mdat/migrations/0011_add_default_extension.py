# Generated by Django 2.1.5 on 2019-02-04 13:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_mdat", "0010_add_appointment_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="voipmdatnumbers",
            name="range_default",
            field=models.IntegerField(default=0),
        ),
    ]
