from datetime import datetime

from django_mdat_customer.django_mdat_customer.models import *


class VoipMdatCarrier(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_mdat_carrier"


class VoipMdatCarrierCodes(models.Model):
    code = models.CharField(max_length=5)
    carrier = models.ForeignKey(
        VoipMdatCarrier, models.CASCADE, db_column="carrier", unique=True
    )

    class Meta:
        db_table = "voip_mdat_carrier_codes"


class VoipMdatCarrierVars(models.Model):
    name = models.CharField(max_length=30)
    value = models.TextField()
    carrier = models.ForeignKey(VoipMdatCarrier, models.CASCADE, db_column="carrier")

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_mdat_carrier_vars"
        unique_together = (("name", "carrier"),)


class VoipMdatNumbers(models.Model):
    NUM_STATUS_DRAFT = 0
    NUM_STATUS_PORTING = 1
    NUM_STATUS_ACTIVE = 2
    NUM_STATUS_DISABLED = 3

    NUM_STATUS_CHOICES = (
        (NUM_STATUS_DRAFT, "NUM_STATUS_DRAFT"),
        (NUM_STATUS_PORTING, "NUM_STATUS_PORTING"),
        (NUM_STATUS_ACTIVE, "NUM_STATUS_ACTIVE"),
        (NUM_STATUS_DISABLED, "NUM_STATUS_DISABLED"),
    )

    NUM_TYPE_SINGLE = 0
    NUM_TYPE_BLOCK = 1

    NUM_TYPE_CHOICES = (
        (NUM_TYPE_SINGLE, "NUM_TYPE_SINGLE"),
        (NUM_TYPE_BLOCK, "NUM_TYPE_BLOCK"),
    )

    id = models.BigAutoField(primary_key=True)
    customer = models.ForeignKey(MdatCustomers, models.CASCADE, db_column="customer")
    reference_code = models.CharField(max_length=40, null=True, blank=True)
    carrier_code = models.ForeignKey(
        VoipMdatCarrierCodes,
        models.CASCADE,
        db_column="carrier_code",
        blank=True,
        null=True,
    )
    country_code = models.IntegerField(default=49)
    national_code = models.IntegerField()
    subscriber_num = models.IntegerField()
    range_start = models.IntegerField(default=0)
    range_end = models.IntegerField(default=0)
    range_default = models.IntegerField(default=0)
    status = models.IntegerField(choices=NUM_STATUS_CHOICES, default=NUM_STATUS_DRAFT)
    appointment_id = models.IntegerField(null=True, blank=True)
    date_added = models.DateTimeField(default=datetime.now)

    def __str__(self):
        zfill_range_end = str(self.range_start).zfill(len(str(self.range_end)))

        number_str = (
            "+"
            + str(self.country_code)
            + " "
            + str(self.national_code)
            + " "
            + str(self.subscriber_num)
        )

        if self.num_type == self.NUM_TYPE_BLOCK:
            number_str = (
                number_str
                + "-"
                + str(self.range_start)
                + " ("
                + zfill_range_end
                + "/"
                + str(self.range_end)
                + ")"
            )

        return number_str

    @property
    def account_id(self):
        account_id = "0" + str(self.national_code) + str(self.subscriber_num)

        if self.range_end > 0:
            account_id = account_id + str(self.range_start).zfill(
                len(str(self.range_end))
            )

        return account_id

    @property
    def block_size(self):
        if self.num_type == self.NUM_TYPE_SINGLE:
            return 1

        return self.range_end - self.range_start + 1

    @property
    def num_type(self):
        if self.range_start != 0 or self.range_end != 0:
            return self.NUM_TYPE_BLOCK

        return self.NUM_TYPE_SINGLE

    class Meta:
        db_table = "voip_mdat_numbers"
        unique_together = (
            (
                "customer",
                "country_code",
                "national_code",
                "subscriber_num",
                "range_start",
                "range_end",
            ),
        )
